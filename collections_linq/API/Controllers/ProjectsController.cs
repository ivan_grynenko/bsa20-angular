﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models;
using AutoMapper;
using BL.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService projectService;
        private readonly IMapper mapper;
        private readonly ILoggerService loggerService;

        public ProjectsController(IProjectService projectService, IMapper mapper, ILoggerService loggerService)
        {
            this.projectService = projectService;
            this.mapper = mapper;
            this.loggerService = loggerService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjectDTO>>> GetProjects()
        {
            return Ok(mapper.Map<IEnumerable<ProjectDTO>>(await projectService.GetAll()));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDTO>> GetProjectById(int id)
        {
            if (id < 1)
                return BadRequest();

            var project = await projectService.GetById(id);

            if (project == null)
                return NotFound();
            else
                return Ok(mapper.Map<ProjectDTO>(project));
        }

        [HttpGet("withtasks")]
        public async Task<ActionResult<IEnumerable<ProjectInfoDTO>>> GetProjectWithTasks()
        {
            return Ok(mapper.Map<IEnumerable<ProjectInfoDTO>>(await projectService.GetProjectWithTasks()));
        }

        [HttpPost("multiple")]
        public async Task<IActionResult> AddProjects([FromBody]IEnumerable<ProjectDTO> newProjects)
        {
            if (newProjects.Any(e => !TryValidateModel(e)))
                return BadRequest();

            await projectService.Add(mapper.Map<IEnumerable<Project>>(newProjects));

            return NoContent();
        }

        [HttpPost()]
        public async Task<IActionResult> AddProject([FromBody] ProjectDTO newProject)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            await projectService.Add(mapper.Map<Project>(newProject));

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Remove(int id)
        {
            if (id < 1)
                return BadRequest();

            var item = await projectService.GetById(id);

            if (item == null)
                return NotFound();

            await projectService.Remove(item);

            return NoContent();
        }

        [HttpPatch("multiple")]
        public async Task<IActionResult> UpdateProjects([FromBody] IEnumerable<ProjectDTO> newProjects)
        {
            await projectService.Update(mapper.Map<IEnumerable<Project>>(newProjects));

            return NoContent();
        }

        [HttpPatch]
        public async Task<IActionResult> UpdateProject([FromBody] ProjectModel newProject)
        {
            await projectService.Update(mapper.Map<Project>(newProject));

            return NoContent();
        }
    }
}
