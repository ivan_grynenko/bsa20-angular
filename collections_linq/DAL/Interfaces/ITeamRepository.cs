﻿using DAL.Base;
using DAL.Models;

namespace DAL.Interfaces
{
    public interface ITeamRepository : IRepository<Team>
    {
    }
}
