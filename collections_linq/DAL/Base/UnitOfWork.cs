﻿using DAL.Models;
using DAL.Repositories;
using System.Threading.Tasks;

namespace DAL.Base
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationContext context;
        private Repository<Project> projectRepository;
        private Repository<Tasks> taskRepository;
        private Repository<Team> teamRepository;
        private Repository<User> userRepository;

        public IRepository<Project> ProjectRepository => projectRepository ?? (projectRepository = new ProjectRepository(context));
        public IRepository<Tasks> TaskRepository => taskRepository ?? (taskRepository = new TaskRepository(context));
        public IRepository<Team> TeamRepository => teamRepository ?? (teamRepository = new TeamRepository(context));
        public IRepository<User> UserRepository => userRepository ?? (userRepository = new UserRepository(context));

        public UnitOfWork(ApplicationContext context)
        {
            this.context = context;
        }

        public async Task SaveChanges()
        {
            await context.SaveChangesAsync();
        }
    }
}
