﻿using Bogus;
using Bogus.Extensions;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Linq;

namespace DAL.Configuration
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users");
            builder.Property(e => e.FirstName)
                .HasMaxLength(50);
            builder.Property(e => e.LastName)
                .HasMaxLength(50);
            builder.HasAlternateKey(e => e.Email);
            builder.HasOne(e => e.Team)
                .WithMany(e => e.Users)
                .HasForeignKey(e => e.TeamId)
                .OnDelete(DeleteBehavior.SetNull);
            builder.HasCheckConstraint("CK_Users_Birthday", "[Birthday] <= getdate()");
            builder.HasCheckConstraint("CK_Users_RegisteredAt", "[RegisteredAt] <= getdate()");
            builder.HasMany(e => e.Projects)
                .WithOne(e => e.Author)
                .HasForeignKey(e => e.AuthorId)
                .OnDelete(DeleteBehavior.SetNull);

            var id = 1;
            var fake = new Faker<User>()
                .RuleFor(u => u.Id, f => id++)
                .RuleFor(u => u.FirstName, (f, u) => f.Name.FirstName())
                .RuleFor(u => u.LastName, (f, u) => f.Name.LastName())
                .RuleFor(u => u.Email, (f, u) => f.Internet.Email(u.FirstName, u.LastName))
                .RuleFor(u => u.Birthday, (f, u) => f.Date.Between(new DateTime(1960, 1, 1), new DateTime(2010, 1, 1)))
                .RuleFor(u => u.RegisteredAt, (f, u) => f.Date.Between(new DateTime(2010, 1, 1), DateTime.Now))
                .RuleFor(u => u.TeamId, f => f.Random.Int(1, 5).OrNull(f));
            var basicData = Enumerable.Range(1, 30)
                .Select(e => fake.Generate());

            builder.HasData(basicData);
        }
    }
}
