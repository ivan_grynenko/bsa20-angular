﻿using DAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BL.Interfaces
{
    public interface ITeamService : IService<Team>
    {
        Task<IEnumerable<(int? Id, string Name, IEnumerable<User> Users)>> GetTeamsOfCertainAge(int minAge);
    }
}